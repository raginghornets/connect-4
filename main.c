#include <stdio.h>

#define NUM_ROWS 6
#define NUM_COLS 7
#define TOTAL_TURNS NUM_ROWS * NUM_COLS

enum state {EMPTY, YELLOW, RED};

struct board {
  int grid[NUM_ROWS][NUM_COLS];
};

void printBoard(struct board b) {
  int r, c;
  for (r = 0; r < NUM_ROWS; r++) {
    for (c = 0; c < NUM_COLS; c++) {
      printf("| %d ", b.grid[r][c]);
    }
    printf("|\n");
  }
}

struct board GameBoard() {
  struct board b;
  int r, c;
  for (r = 0; r < NUM_ROWS; r++) {
    for (c = 0; c < NUM_COLS; c++) {
      b.grid[r][c] = EMPTY;
    }
  }
  return b;
}

struct board insertPiece(struct board b, int col, int color) {
  int row = 0;
  if (col < 0 || col > NUM_COLS) {
    printf("Invalid column.\n");
    return b;
  }

  while (b.grid[row + 1][col] == EMPTY && row + 1 < NUM_ROWS) {
    row++;
  }

  b.grid[row][col] = color;
  return b;
}

int getUserInt() {
  int column;
  printf("Column: ");
  scanf("%d", &column);
  return column;
}

int alternateColor(int color) {
  if (color == YELLOW) {
    return RED;
  } else {
    return YELLOW;
  }
}

int main(void) {
  struct board b = GameBoard();

  int turn = 0;
  int current_color = YELLOW;
  printBoard(b);
  while (turn < TOTAL_TURNS) {
    b = insertPiece(b, getUserInt(), current_color);
    current_color = alternateColor(current_color);
    printBoard(b);
  }

  printBoard(b);
  return 0;
}
